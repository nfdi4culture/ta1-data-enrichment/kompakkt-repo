import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AccountService } from 'src/app/services';

@Component({
  selector: 'app-auth-dialog',
  templateUrl: './auth-dialog.component.html',
  styleUrls: ['./auth-dialog.component.scss'],
})
export class AuthDialogComponent {
  public waitingForResponse = false;
  public loginFailed = false;

  public data = {
    username: '',
    password: '',
  }

  constructor(
    public dialogRef: MatDialogRef<AuthDialogComponent>,
    public account: AccountService,
    @Inject(MAT_DIALOG_DATA) public concern: string,
  ) {}

  public async clickedLogin() {
    const { username, password } = this.data;

    this.waitingForResponse = true;
    this.dialogRef.disableClose = true;
    console.debug('Logging in with', username, password);

    // first version with let success

    let success =  false;
    try {
      success = await this.account.attemptLogin(username, password);
    } catch (e) {
      console.debug('Login failed', e);
      this.loginFailed = true;
    }

    this.dialogRef.disableClose = false;
    this.waitingForResponse = false;

    this.loginFailed = !success;
    console.debug('Login attempt', success ? 'successful' : 'failed');
    if (!success) return;

    this.dialogRef.close({ username, password });
  }
}
