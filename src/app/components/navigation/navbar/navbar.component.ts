import {Component, EventEmitter, AfterViewInit, Output, ViewChild, Input} from '@angular/core';
import { Router } from '@angular/router';
import { MatProgressBar } from '@angular/material/progress-bar';
import { TranslateService } from '@ngx-translate/core';

import {
  AccountService,
  ProgressBarService,
  DialogHelperService,
  EventsService,
  SelectHistoryService
} from 'src/app/services';
import {UserRank} from "~common/enums";
import {
  ICompilation,
  IEntity,
  IUserData,
  isEntity,
  isCompilation,
} from 'src/common';
import {ConfirmationDialogComponent, UploadApplicationDialogComponent} from "~dialogs";
import {MatDialog} from "@angular/material/dialog";
import {AddEntityWizardComponent} from "~wizards";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements AfterViewInit {
  @Output() public sidenavToggle = new EventEmitter();
  @Input() element: IEntity | ICompilation | undefined;

  public languages = this.translate.getLangs();
  public userData: IUserData | undefined;
  public isEntity = isEntity;
  public isCompilation = isCompilation;

  @ViewChild('progressBar')
  private progressBar: undefined | MatProgressBar;

  constructor(
    private account: AccountService,
    public translate: TranslateService,
    private progress: ProgressBarService,
    private dialog: MatDialog,
    private dialogHelper: DialogHelperService,
    private router: Router,
    private events: EventsService,
    public selectHistory: SelectHistoryService,
  ) {    this.account.userData$.subscribe(newData => {
    if (!newData) return;
    this.userData = newData;
  });
  }

  get isAuthenticated$() {
    return this.account.isAuthenticated$;
  }

  get isUploader() {
    return this.userData?.role === UserRank.admin || this.userData?.role === UserRank.uploader;
  }


  get isAdmin$() {
    return this.account.isAdmin$;
  }

  ngAfterViewInit() {
    if (this.progressBar) {
      this.progress.setProgressBar(this.progressBar);
    }
  }

  public logout() {
    this.account.logout().then(() => this.router.navigate(['/']));
  }

  public onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  public openLoginDialog() {
    this.dialogHelper.openLoginDialog();
  }

  public openRegisterDialog() {
    this.dialogHelper.openRegisterDialog();
  }

  public openUploadApplication() {
    if (!this.userData) {
      alert('Not logged in');
      return;
    }
    const dialogRef = this.dialog.open(UploadApplicationDialogComponent, {
      data: this.userData,
      disableClose: true,
    });

    dialogRef.backdropClick().subscribe(async () => {
      const confirm = this.dialog.open(ConfirmationDialogComponent, {
        data: 'Do you want to cancel your application?',
      });
      await confirm
          .afterClosed()
          .toPromise()
          .then(shouldClose => {
            if (shouldClose) {
              dialogRef.close();
            }
          });
    });
  }

  public uploadRequested() {
    if (!this.userData) return false;
    return this.userData.role === UserRank.uploadrequested;
  }

  public async openEntityCreation(entity?: IEntity) {
    const dialogRef = this.dialog.open(AddEntityWizardComponent, {
      data: entity ? entity : undefined,
      disableClose: true,
    });

    dialogRef
        .afterClosed()
        .toPromise()
        .then(result => {
          this.events.updateSearchEvent();
          if (result && this.userData && this.userData.data.entity) {
            const index = (this.userData.data.entity as IEntity[]).findIndex(
                _en => result._id === _en._id,
            );
            if (index === -1) return;
            this.userData.data.entity.splice(index, 1, result as IEntity);
            // this.updateFilter();
          }
        });
  }

  public async openCompilationCreation(compilation?: ICompilation) {
    const dialogRef = this.dialogHelper.openCompilationWizard(compilation);
    dialogRef
        .afterClosed()
        .toPromise()
        .then(result => {
          this.events.updateSearchEvent();
          if (result && this.userData && this.userData.data.compilation) {
            if (compilation) {
              const index = (this.userData.data.compilation as ICompilation[]).findIndex(
                  comp => comp._id === result._id,
              );
              if (index === -1) return;
              this.userData.data.compilation.splice(index, 1, result as ICompilation);
            } else {
              (this.userData.data.compilation as ICompilation[]).push(result as ICompilation);
            }
          }
        });
  }

  public navigate(element: IEntity | ICompilation) {
    // Parent will load and fetch relevant data
    this.element = undefined;

    return this.router.navigateByUrl(
        `/${isEntity(element) ? 'entity' : 'compilation'}/${element._id}`,
    );
  }
}
