import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AccountService, BackendService } from 'src/app/services';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss'],
})
export class RegisterDialogComponent {
  public error = '';

  public form = new FormGroup({
    prename: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    mail: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    passwordRepeat: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });

  public waitingForResponse = false;

  constructor(
    private backend: BackendService,
    public dialogRef: MatDialogRef<RegisterDialogComponent>,
    private account: AccountService,
  ) {}

  public async trySubmit() {
    this.error = '';
    const { username, password, prename, surname, passwordRepeat } = this.form.value;
    if (password !== passwordRepeat) {
      this.error = 'Passwords do not match';
      return;
    }

    this.waitingForResponse = true;
    this.dialogRef.disableClose = true;
    const registerSuccess = await this.backend
      .registerAccount({ ...this.form.value, fullname: `${prename} ${surname}` })
      .catch((e: HttpErrorResponse) => {
        console.log('Error', e);
        this.error = e.error;
        return false;
      });
    console.log('Response', registerSuccess);
    if (!registerSuccess) {
      this.waitingForResponse = false;
      this.dialogRef.disableClose = false;
      return;
    }

    const loginSuccess = await this.account.attemptLogin(username, password);

    this.dialogRef.disableClose = false;
    this.waitingForResponse = false;
    if (!loginSuccess) return;

    this.dialogRef.close({ username, password });
  }
}
