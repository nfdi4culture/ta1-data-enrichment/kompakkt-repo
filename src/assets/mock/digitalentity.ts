import { IDigitalEntity } from 'src/common';

export const mockDigitalEntity: IDigitalEntity = {
  _id: '5e4681d54dd0839e20685ef5',
  label: {
      en: 'Ushabti in form of a mummy for Djehutihotep',
  },
  description:
    'The ushabti is 24,7 cm high, made of limestone and painted. It dates into the New Kingdom, 18th dynasty (ar. 1532-1292 BCE). The figurine was made for the deceased, in order to take over painful work in the afterworld.\n\nThis model was processed with the Structure from Motion technique provided by Agisoft Photoscan. The images were taken with the kind permission of the Egyptology department of the University of Cologne.',
  biblioRefs: [
    {
      description: '',
      value:
        'Blom-Böer, I. 1999: Aegyptiaca Coloniensia. Katalog der ägyptischen Objekte des Seminars für Ägyptologie der Universität zu Köln, Köln, 23-27.',
    },
  ],
  creation: [
    {
      technique: 'Structure from Motion',
      program: 'Agisoft Photoscan',
      equipment: 'Nikon D200',
      date: '20.10.2017',
    },
  ],
  dimensions: [
    {
      type: 'cm',
      value: '24,7',
      name: 'Height',
    },
  ],
  discipline: ['Archaeology', 'Egyptology'],
  externalId: [],
  externalLink: [],
  files: [],
  // institutions: [
  //   {
  //     _id: '5e46819f4dd0839e20685ef4',
  //     addresses: {
  //       '5e78f5b7bd2980492e0373cd': {
  //         _id: '6053f6d56f1b1ffe718a1481',
  //         building: '125',
  //         city: 'Cologne',
  //         country: 'Germany',
  //         creation_date: 1581679007040,
  //         number: '30',
  //         postcode: '50937',
  //         street: 'Kerpener Straße',
  //       },
  //     },
  //     name: 'Institute of Archaeology',
  //     notes: {},
  //     roles: {
  //       '5e78f5b7bd2980492e0373cd': ['CREATOR', 'EDITOR', 'DATA_CREATOR', 'CONTACT_PERSON'],
  //     },
  //     university: 'University of Cologne',
  //   },
  // ],
  techniques: [],
  software: [],
  equipment: [],
  creationDate: "1970-01-01",
  externalLinks: [],
  bibliographicRefs: [],
  physicalObjs: [],
  licence: 452,
  metadata_files: [],
  objecttype: 'model',
  other: [],
  agents: [
    {
      id: '5e4681d54dd0839e20685ef5',
      internalID: '5e4681d54dd0839e20685ef5',
      label: {
          en: 'Sebastian Hageneuer',
      },
      description: '',
      role: 520,
      roleTitle: "Rightsowner",
    },
  ],
  statement: '',
  tags: [
    {
      _id: '5e4682271ba8f3627ab03118',
      value: 'Archaeology',
    },
    {
      _id: '5e78f84ee49048e628747ae6',
      value: 'Egyptology',
    },
    {
      _id: '5e78f84ee49048e628747aea',
      value: 'Uschabti',
    },
    {
      _id: '5e78f84ee49048e628747aee',
      value: 'Figurine',
    },
    {
      _id: '5e4682271ba8f3627ab03124',
      value: 'SfM',
    },
    {
      _id: '5e4682271ba8f3627ab0311c',
      value: 'Terracotta',
    },
  ],
  type: '',
  hierarchies: [{
    parents: [
      { id: 'Q52', label: { 'en': 'Weikersheim castle complex' } },
      { id: 'Q47', label: { 'en': 'Weikersheim castle' } },
      { id: 'Q49', label: { 'en': 'Great hall' } },
      { id: 'Q345', label: { 'en': 'Access room sequences' } },
      { id: 'Q64', label: { 'en': 'Dining room' } }
    ],
    siblings: [
      { id: 'Q62', label: { 'en': "Knight's chamber" }, media: undefined },
      { id: 'Q63', label: { 'en': 'Hallway' }, media: undefined },
      { id: 'Q64', label: { 'en': 'Dining room' }, media: 'Q446' }
    ]
  },],
};
